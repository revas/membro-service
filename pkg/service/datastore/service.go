package datastore

import (
	"context"
	"crypto/rand"

	"cloud.google.com/go/datastore"
	"github.com/go-kit/kit/log"
	"github.com/oklog/ulid"

	"gitlab.com/revas/membro-service/pkg"
)

type GoogleDatastoreMembroService struct {
	Logger log.Logger
	Client *datastore.Client
}

// Ensure InMemoryMembroService implements the membro.MembroService interface.
var _ membro.MembroService = &GoogleDatastoreMembroService{}

func (svc *GoogleDatastoreMembroService) CountMembers(ctx context.Context, orgID string, offset int, limit int, status string) (int, error) {
	realmKey := datastore.NameKey("Realm", orgID, nil)
	query := datastore.NewQuery("Member").
		Filter("Status =", status).
		Ancestor(realmKey).
		KeysOnly()
	count, err := svc.Client.Count(ctx, query)
	if err != nil {
		return 0, err
	}
	return count, err
}

func (svc *GoogleDatastoreMembroService) QueryMembers(ctx context.Context, orgID string, offset int, limit int, status string, members []*membro.Member) error {
	realmKey := datastore.NameKey("Realm", orgID, nil)
	query := datastore.NewQuery("Member").
		Offset(offset).
		Limit(limit).
		Filter("Status =", status).
		Ancestor(realmKey).
		KeysOnly()

	keys, err := svc.Client.GetAll(ctx, query, nil)
	if err != nil {
		return err
	}
	err = svc.Client.GetMulti(ctx, keys, members)
	return err
}

func (svc *GoogleDatastoreMembroService) GetMembers(ctx context.Context, orgID string, memberIDs []string, members []*membro.Member) error {
	realmKey := datastore.NameKey("Realm", orgID, nil)
	var membersKeys []*datastore.Key
	for _, memberID := range memberIDs {
		membersKeys = append(membersKeys, datastore.NameKey("Member", memberID, realmKey))
	}
	err := svc.Client.GetMulti(ctx, membersKeys, members)
	return err
}

func (svc *GoogleDatastoreMembroService) UpdateMembers(ctx context.Context, orgID string, memberIDs []string, members []*membro.Member) error {
	realmKey := datastore.NameKey("Realm", orgID, nil)
	var membersKeys []*datastore.Key
	for _, member := range members {
		if member.ID == "" {
			member.ID = ulid.MustNew(ulid.Now(), rand.Reader).String()
			member.Status = "PENDING"
		}
		memberKey := datastore.NameKey("Member", member.ID, realmKey)
		membersKeys = append(membersKeys, memberKey)
	}
	_, err := svc.Client.PutMulti(ctx, membersKeys, members)
	return err
}
