package membro

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

type QueryMembersRequest struct {
	Organizations []*OrganizationPayload `json:"organizations"`
}

type QueryMembersResponse struct {
	Organizations []*OrganizationPayload `json:"organizations"`
	Err           string                 `json:"err,omitempty"`
}

type GetMembersRequest struct {
	Organizations []*OrganizationPayload `json:"organizations"`
}

type GetMembersResponse struct {
	Organizations []*OrganizationPayload `json:"organizations"`
	Err           string                 `json:"err,omitempty"`
}

type UpdateMembersRequest struct {
	Organizations []*OrganizationPayload `json:"organizations"`
}

type UpdateMembersResponse struct {
	Organizations []*OrganizationPayload `json:"organizations"`
	Err           string                 `json:"err,omitempty"`
}

type OrganizationPayload struct {
	ID      string    `json:"id"`
	Members []*Member `json:"members"`
	Filter  *Member   `json:"filter"`
	Limit   int       `json:"limit"`
	Offset  int       `json:"offset"`
	Count   int       `json:"count"`
	Error   string    `json:"error"`
}

type Endpoints struct {
	QueryMembersEndpoint  endpoint.Endpoint
	GetMembersEndpoint    endpoint.Endpoint
	UpdateMembersEndpoint endpoint.Endpoint
}

func MakeQueryMembersEndpoint(svc MembroService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(QueryMembersRequest)
		for _, org := range req.Organizations {
			count, err := svc.CountMembers(ctx, org.ID, org.Offset, org.Limit, org.Filter.Status)
			org.Count = count

			org.Members = make([]*Member, (count - org.Offset) % org.Limit)
			for i := range org.Members {
				org.Members[i] = &Member{}
			}
			err = svc.QueryMembers(ctx, org.ID, org.Offset, org.Limit, org.Filter.Status, org.Members)
			if err != nil {
				org.Error = err.Error()
			}
		}
		return QueryMembersResponse{req.Organizations, ""}, nil
	}
}

func MakeGetMembersEndpoint(svc MembroService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetMembersRequest)
		for _, org := range req.Organizations {
			var memberIDs []string
			for _, member := range org.Members {
				memberIDs = append(memberIDs, member.ID)
			}
			err := svc.GetMembers(ctx, org.ID, memberIDs, org.Members)
			if err != nil {
				org.Error = err.Error()
			}
		}
		return GetMembersResponse{req.Organizations, ""}, nil
	}
}

func MakeUpdateMembersEndpoint(svc MembroService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(UpdateMembersRequest)
		for _, org := range req.Organizations {
			var memberIDs []string
			for _, member := range org.Members {
				memberIDs = append(memberIDs, member.ID)
			}
			err := svc.UpdateMembers(ctx, org.ID, memberIDs, org.Members)
			if err != nil {
				org.Error = err.Error()
			}
		}
		return UpdateMembersResponse{req.Organizations, ""}, nil
	}
}
