package membro

import (
	"context"
	"time"
)

type MemberBirth struct {
	Date  time.Time `json:"date"`
	Place string    `json:"place"`
}

type MemberAddress struct {
	Place string `json:"place"`
	OLC   string `json:"olc"`
}

type Member struct {
	ID       string        `json:"id"`
	Token    string        `json:"token"`
	Status   string        `json:"status"`
	Internal string        `json:"internal"`
	Name     string        `json:"name"`
	Code     string        `json:"code"`
	Birth    MemberBirth   `json:"birth" datastore:",flatten"`
	Address  MemberAddress `json:"address" datastore:",flatten"`
	Document string        `json:"document"`
	Email    string        `json:"email"`
	Phone    string        `json:"phone"`
}

type MembroService interface {
	CountMembers(ctx context.Context, orgID string, offset int, limit int, status string) (int, error)
	QueryMembers(ctx context.Context, orgID string, offset int, limit int, status string, members []*Member) error
	GetMembers(ctx context.Context, orgID string, memberIDs []string, members []*Member) error
	UpdateMembers(ctx context.Context, orgID string, memberIDs []string, members []*Member) error
	// VerifyRequest(ctx context.Context, orgs []*Organization) ([]*Organization, error)
}
