package http

import (
	"context"
	"encoding/json"
	"net/http"

	kitjwt "github.com/go-kit/kit/auth/jwt"
	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

	"gitlab.com/revas/membro-service/pkg"
)

func DecodeQueryMembersRequest(_ context.Context, request *http.Request) (interface{}, error) {
	var decoded membro.QueryMembersRequest
	if err := json.NewDecoder(request.Body).Decode(&decoded); err != nil {
		return nil, err
	}
	return decoded, nil
}

func DecodeGetMembersRequest(_ context.Context, request *http.Request) (interface{}, error) {
	var decoded membro.GetMembersRequest
	if err := json.NewDecoder(request.Body).Decode(&decoded); err != nil {
		return nil, err
	}
	return decoded, nil
}

func DecodeUpdateMembersRequest(_ context.Context, request *http.Request) (interface{}, error) {
	var decoded membro.UpdateMembersRequest
	if err := json.NewDecoder(request.Body).Decode(&decoded); err != nil {
		return nil, err
	}
	return decoded, nil
}

func EncodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func OnOptionsMethodReturn(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}

func MakeHandler(logger log.Logger, endpoints membro.Endpoints) http.Handler {
	options := []kithttp.ServerOption{
		kithttp.ServerBefore(kitjwt.HTTPToContext()),
		kithttp.ServerErrorLogger(logger),
	}

	queryMembersHandler := kithttp.NewServer(
		endpoints.QueryMembersEndpoint,
		DecodeQueryMembersRequest,
		EncodeResponse,
		options...,
	)

	getMembersHandler := kithttp.NewServer(
		endpoints.GetMembersEndpoint,
		DecodeGetMembersRequest,
		EncodeResponse,
		options...,
	)

	createMembersHandler := kithttp.NewServer(
		endpoints.UpdateMembersEndpoint,
		DecodeUpdateMembersRequest,
		EncodeResponse,
		options...,
	)

	r := mux.NewRouter()

	r.Handle("/membro.QueryMembers/", OnOptionsMethodReturn(queryMembersHandler)).Methods("POST")
	r.Handle("/membro.GetMembers/", OnOptionsMethodReturn(getMembersHandler)).Methods("POST")
	r.Handle("/membro.UpdateMembers/", OnOptionsMethodReturn(createMembersHandler)).Methods("POST")

	return r
}
